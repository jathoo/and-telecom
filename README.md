## Prerequisites

please install Java 11 to run the application 


### Running the application 

To run the application use 

./gradlew bootRun 


to run tests use 

./gradlew test


## Using the application

1. start the application 
2. go to http://localhost:8080/swagger-ui.html
3. click on the customer-controller tab 
4. select an  end point 


### Examples 

you need a user id to find out all the phone numbers for a single customer ( Eg:  "id1") 

use "078723" to activate this phone number .


Example video : https://www.loom.com/share/1adac15e0d6142449de42bad93c48dd1

Please note : CustomerUtil class is used to populate customers as database cannot be used in the task.