package com.anddigital.telecom.util;

import com.anddigital.telecom.Data.Customer;
import com.anddigital.telecom.Data.PhoneNumber;

import java.util.List;

public class CustomerUtil {

    public static List<Customer> GenerateCustomers(){
        Customer rob =  new Customer("id1","Rob",List.of(new PhoneNumber("078723"), new PhoneNumber("098732332"), new PhoneNumber("0982213")));
        Customer mike =  new Customer("id2","Mike",List.of(new PhoneNumber("07663443"), new PhoneNumber("0923343"), new PhoneNumber("098323")));
        Customer userWIthOutNumber = new Customer("id3","Hulk");
        userWIthOutNumber.addPhoneNumber(new PhoneNumber("000073"));
        return  List.of(rob,mike,userWIthOutNumber);
    }
}
