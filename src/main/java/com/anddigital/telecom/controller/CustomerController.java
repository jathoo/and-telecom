package com.anddigital.telecom.controller;

import com.anddigital.telecom.Data.PhoneNumber;
import com.anddigital.telecom.service.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/phone-numbers")
    public Map phoneNumbers() {
        return Map.of("phoneNumbers", customerService.getAllPhoneNumbersAsString());
    }

    @GetMapping("/customer/phone-numbers")
    public Map customerPhoneNumbers(@RequestParam("userId") String id) {
        return Map.of(id, customerService.getPhoneNumberFor(id));
    }

    @PostMapping("/customer/phone-number/activate")
    public PhoneNumber activatePhoneNumber(@RequestBody String phoneNumber) {
        return customerService.activateNumber(phoneNumber);
    }

}
