package com.anddigital.telecom.service;

import com.anddigital.telecom.Data.Customer;
import com.anddigital.telecom.Data.PhoneNumber;
import com.anddigital.telecom.util.CustomerUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {


    private List<Customer> getAllCustomers() {
        return CustomerUtil.GenerateCustomers();
    }

    public List<String> getAllPhoneNumbersAsString() {
        return getAllPhoneNumbers().stream().map(PhoneNumber::getNumber).collect(Collectors.toList());
    }

    public List<String> getPhoneNumberFor(String id) {
        return getAllCustomers()
                .stream()
                .filter(item -> item.getId().equals(id))
                .map(Customer::getPhoneNumbersFilteredByNumber)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    public PhoneNumber activateNumber(String number) {
        PhoneNumber phoneNumber = findNumber(number);
        phoneNumber.activate();
        return phoneNumber;
    }

    private PhoneNumber findNumber(String number) {
        return getAllPhoneNumbers()
                .stream()
                .filter(item -> item.getNumber().equals(number))
                .findAny().orElseThrow();
    }

    private List<PhoneNumber> getAllPhoneNumbers() {
        return getAllCustomers()
                .stream()
                .map(Customer::getPhoneNumbers)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

}
