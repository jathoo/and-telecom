package com.anddigital.telecom.Data;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Customer {

    private String id;
    private String name;
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();

    public Customer(String id, String name, List<PhoneNumber> phoneNumbers) {
        this(id, name);
        this.phoneNumbers = phoneNumbers;
    }

    public Customer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addPhoneNumber(PhoneNumber phoneNumber) {
        phoneNumbers.add(phoneNumber);
    }

    public String getId() {
        return this.id;
    }

    public List<String> getPhoneNumbersFilteredByNumber() {
        return phoneNumbers.stream().map(PhoneNumber::getNumber).collect(Collectors.toList());

    }

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }
}
