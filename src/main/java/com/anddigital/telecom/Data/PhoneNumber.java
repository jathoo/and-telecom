package com.anddigital.telecom.Data;

public class PhoneNumber {
    private String number;
    private boolean activate = false;

    public PhoneNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return this.number;
    }

    public void activate() {
        this.activate = true;
    }

    public boolean getStatus() {
        return this.activate;
    }
}
