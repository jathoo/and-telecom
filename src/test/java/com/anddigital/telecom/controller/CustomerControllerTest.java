package com.anddigital.telecom.controller;

import com.anddigital.telecom.Data.PhoneNumber;
import com.anddigital.telecom.service.CustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@RunWith(SpringRunner.class)
public class CustomerControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;


    @Test
    public void getAllPhoneNumbers() throws Exception {
        List<String> phoneNumbers = List.of("22323", "232323", "23232323");

        when(customerService.getAllPhoneNumbersAsString()).thenReturn(phoneNumbers);

        mockMvc.perform(get("/api/customers/phone-numbers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.phoneNumbers", is(phoneNumbers)));

        verify(customerService).getAllPhoneNumbersAsString();
    }

    @Test
    public void getAllPhoneNumberOfASingleCustomer() throws Exception {

        String id = "id1";
        List<String> phoneNumbers = List.of("22323", "232323");
        when(customerService.getPhoneNumberFor(id)).thenReturn(phoneNumbers);
        mockMvc.perform(
                get("/api/customers/customer/phone-numbers")
                        .param("userId", id)
        ).andExpect(
                status().isOk()
        ).andExpect(
                jsonPath("$.id1", is(phoneNumbers))
        );

        verify(customerService).getPhoneNumberFor(id);

    }

    @Test
    public void activateAPhoneNumber() throws Exception {
        PhoneNumber phoneNumber = new PhoneNumber("123");
        phoneNumber.activate();

        when(customerService.activateNumber("123")).thenReturn(phoneNumber);

        mockMvc.perform(
                post("/api/customers/customer/phone-number/activate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("123"))
                .andExpect(
                        status().isOk()
                ).andExpect(
                jsonPath("$.status", is(true))
        );

        verify(customerService).activateNumber("123");
    }

}
