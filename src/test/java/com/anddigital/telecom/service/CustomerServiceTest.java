package com.anddigital.telecom.service;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;


public class CustomerServiceTest {

    private CustomerService customerService = new CustomerService();

    @Test
    public void returnAllThePhoneNumbers() {
        assertThat(customerService.getAllPhoneNumbersAsString()).contains("078723", "098732332", "0982213", "07663443", "0923343", "098323", "000073");
    }

    @Test
    public void returnCustomerPhoneNumberFor() {
        assertThat(customerService.getPhoneNumberFor("id1")).contains("078723", "098732332", "0982213");
    }

    @Test
    public void returnEmptyIfNoCustomerFound() {
        assertThat(customerService.getPhoneNumberFor("id4")).isEmpty();
    }

    @Test
    public void activatePhoneNumber() {
        assertThat(customerService.activateNumber("078723").getStatus()).isTrue();
    }

    @Test(expected = NoSuchElementException.class)
    public void invalidPhoneNumberThrowsException() {
        customerService.activateNumber("xxx");
    }
}
